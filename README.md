**Google Books Code Challenge**

This is a simple Google Books app that reads from the Google Books API, parses the response and outputs the first 10 books as HTML.

Book information shown is title, authors, image and description.

**Requires an API Key for Google Books**

To run, you'll need to create a Google Books project and get an API key from the Google Developer Console:
https://console.developers.google.com/iam-admin/projects

Include the API key in your running environment.

**Using Ruby version 2.4.1**

**Example running on Heroku**

https://googlebooksapp.herokuapp.com/books