class BooksController < ApplicationController

  def index
    @books = Book.all.order(:title)
    if params[:query]
      @query = params[:query]
      @books = Book.search(@query)
    end
  end

end
