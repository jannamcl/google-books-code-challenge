require 'google_books/client'

class Book < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :authors
  validates_presence_of :description
  validates_presence_of :image
  validates_presence_of :link
  validates :isbn_13, presence: true, uniqueness: true

  def self.search(query)
    GoogleBooks::Client.new.find_books(query)
  end

  def self.create_book(volumeInfo)
    @title = volumeInfo['title'] || ""
    @authors = volumeInfo['authors'] || []
    @description = volumeInfo['description'] || ""
    @image = volumeInfo.dig('imageLinks','thumbnail') || ""
    @link = volumeInfo['previewLink'] || ""
    @isbn_13 = volumeInfo['industryIdentifiers'].find{ |i|
        i['type'] == "ISBN_13"
      }['identifier'] rescue nil

    Book.create(
        title: @title,
        authors: @authors,
        description: @description,
        image: @image,
        link: @link,
        isbn_13: @isbn_13
      )
  end

  def print_authors
    self.authors.join(",\n") unless self.authors.nil?
  end
end
