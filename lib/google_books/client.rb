require 'rest-client'
require 'client'

class GoogleBooks::Client < Client

  def initialize
    super("https://www.googleapis.com/")
  end

  def find_books(query)
    headers = {
      params: {
        q: query,
        key: books_api_key
      }
    }

    response = request(:get, books_path, nil, headers, :json)

    parse_response(response) unless response.nil?
  end

  private

  def books_path
    "books/v1/volumes"
  end

  def books_api_key
    @key ||= GOOGLE_BOOKS_API_KEY
  end

  def parse_response(response)
    return nil unless response['kind'] == "books#volumes"

    total_books = response['totalItems'].to_i
    books = []

    if total_books > 0
      items = response['items']

      items.each do |item|
        if item['kind'] == "books#volume"
          book = create_book(item['volumeInfo'])

          books << book unless book.nil?
        end
      end
    end

    books
  end

  def create_book(item)
    Book.create_book(item)
  end

end
