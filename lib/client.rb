require 'rest-client'
require 'json'

class Client

  def initialize(base_uri)
    @base_uri = base_uri
  end

  def request(method, path="", payload={}, headers={}, request_type=:json)
    headers['Accept'] = request_type
    if payload
      headers['Content-Type'] = request_type
      payload = payload.to_json
    end
    response = execute_request(:get, path, payload, headers)

    unless response.code == 200
      #report in log
      return nil
    end

    if request_type == :json
      response = JSON.parse(response)
    end

    response
  end

  private

  def execute_request(method, path="", payload={}, headers={})
      request = {
        method: method,
        url: @base_uri+path,
        timeout: 10,
        headers: headers
      }
      if payload
        request['payload'] = payload
      end

      RestClient::Request.execute(
        request
      )
    rescue RestClient::ExceptionWithResponse => exception
      exception.response
    rescue Exception => exception
      exception
  end

end
