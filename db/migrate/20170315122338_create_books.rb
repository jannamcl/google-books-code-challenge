class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :title
      t.string :authors, array: true, default: []
      t.string :description
      t.string :image
      t.string :link
      t.string :isbn_13

      t.timestamps
    end
  end
end
